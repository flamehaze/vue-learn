import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/books/:id',
            name: 'books',
            component: () => import('./views/Book')
        },
        {
            path: '/homepage',
            name: 'homepage',
            component: () => import('./views/Homepage.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Login.vue')
        },
        {
            path: '/protected',
            name: 'protected',
            component: () => import(/* webpackChunkName: "about" */ './views/Protected.vue')
        }
    ]
});


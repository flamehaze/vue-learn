import Vue from 'vue';
import App from './views/App.vue';
import router from "./router";
import { createProvider } from './vue-apollo'

Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    router,
    apolloProvider: createProvider(),
    // i18n,
    render: h => h(App),
});


// new Vue({
//     router,
//     apolloProvider: createProvider(),
//     render: h => h(App)
// }).$mount('#app');

<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(TBL_CATEGORIES)->truncate();

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Marketing'
        ]);

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Business'
        ]);

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Finance'
        ]);

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Entrepreneurship'
        ]);

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Science'
        ]);

        DB::table(TBL_CATEGORIES)->insert([
            'name' => 'Biography'
        ]);
    }
}
